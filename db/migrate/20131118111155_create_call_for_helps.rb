class CreateCallForHelps < ActiveRecord::Migration
  def change
    create_table :call_for_helps do |t|
      t.string :name
      t.float  :lng
      t.float  :lat
      t.string :problem
      t.timestamps
    end
  end
end
