class CreateOperators < ActiveRecord::Migration
  def change
    create_table :operators do |t|
      t.string :operator
      t.string :phone
      t.boolean :logedin

      t.timestamps
    end
  end
end
