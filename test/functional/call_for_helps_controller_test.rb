require 'test_helper'

class CallForHelpsControllerTest < ActionController::TestCase
  setup do
    @call_for_help = call_for_helps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:call_for_helps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create call_for_help" do
    assert_difference('CallForHelp.count') do
      post :create, call_for_help: {  }
    end

    assert_redirected_to call_for_help_path(assigns(:call_for_help))
  end

  test "should show call_for_help" do
    get :show, id: @call_for_help
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @call_for_help
    assert_response :success
  end

  test "should update call_for_help" do
    put :update, id: @call_for_help, call_for_help: {  }
    assert_redirected_to call_for_help_path(assigns(:call_for_help))
  end

  test "should destroy call_for_help" do
    assert_difference('CallForHelp.count', -1) do
      delete :destroy, id: @call_for_help
    end

    assert_redirected_to call_for_helps_path
  end
end
