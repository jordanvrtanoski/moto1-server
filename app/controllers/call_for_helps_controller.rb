class CallForHelpsController < ApplicationController
  # GET /call_for_helps
  # GET /call_for_helps.json
  def index
    @call_for_helps = CallForHelp.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @call_for_helps }
    end
  end

  # GET /call_for_helps/1
  # GET /call_for_helps/1.json
  def show
    @call_for_help = CallForHelp.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @call_for_help }
    end
  end

  # GET /call_for_helps/new
  # GET /call_for_helps/new.json
  def new
    @call_for_help = CallForHelp.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @call_for_help }
    end
  end

  # GET /call_for_helps/1/edit
  def edit
    @call_for_help = CallForHelp.find(params[:id])
  end

  # POST /call_for_helps
  # POST /call_for_helps.json
  def create
    @call_for_help = CallForHelp.new(params[:call_for_help])

    respond_to do |format|
      if @call_for_help.save
        # Send SMS message
        send_sms()
        #
        format.html { redirect_to @call_for_help, notice: 'Call for help was successfully created.' }
        format.json { render json: @call_for_help, status: :created, location: @call_for_help, message: 'Call for help was successfully created.' }
      else
        format.html { render action: "new" }
        format.json { render json: @call_for_help.errors, status: :unprocessable_entity }
      end
    end
  end

  def send_sms
    Rails.logger.info("Connecting to SMS service...")
    message = 'User %s is having %s problem @%0.6f, %0.6f. See details http://ec2-54-235-172-210.compute-1.amazonaws.com:3029/call_for_helps/%i' % [@call_for_help.name, @call_for_help.problem, @call_for_help.lng, @call_for_help.lat, @call_for_help.id]
    api = Clickatell::API.authenticate('3451859', 'jordan.vrtanoski.tsba', 'TsBa123123')
    @all_operators=Operator.find_all_by_logedin(true)
    @all_operators.each do |operator|
#      message_id = api.send_message(operator.phone, message, :from => '+971559342320')
      message_id = api.send_message(operator.phone, message)
      Rails.logger.info("Message sent to %s on %s with message_id %s" % [operator.operator, operator.phone, message_id])
    end
  end

  # PUT /call_for_helps/1
  # PUT /call_for_helps/1.json
  def update
    @call_for_help = CallForHelp.find(params[:id])

    respond_to do |format|
      if @call_for_help.update_attributes(params[:call_for_help])
        format.html { redirect_to @call_for_help, notice: 'Call for help was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @call_for_help.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /call_for_helps/1
  # DELETE /call_for_helps/1.json
  def destroy
    @call_for_help = CallForHelp.find(params[:id])
    @call_for_help.destroy

    respond_to do |format|
      format.html { redirect_to call_for_helps_url }
      format.json { head :no_content }
    end
  end
end
